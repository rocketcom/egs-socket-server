var WebSocketServer = require("ws").Server;
var fs = require("fs");
var config = JSON.parse(fs.readFileSync("./config/config.json", "utf-8"));
var Chance = require('chance');
var chance = new Chance();
var wss = new WebSocketServer({ port: config.ports.satellite2 });
var seedFile = "./data/frequency-power.csv";
var rawFileData = fs.readFileSync(seedFile, "utf-8");
var rawArray = rawFileData.split("\r\n"); // Split on new lines (including Excel garbage \r)
var operandLabels = ["power", "temperature"];
var numOperands = 6;
var numModems = 24;

console.log("Satellite Web Socket Server running on port %i", config.ports.satellite2);

wss.on('connection', function(ws) {
  var telemetryData = {};

  setInterval(
    function() {
      telemetryData.satelliteName = config.satellites.satellite2_name;
      var graphData = [];
      for (var j = 0; j < rawArray.length; j++) {
        rawData = rawArray[j];
        var rawDataArray = rawData.split(",");
        var graphObj = {};
        graphObj.f = parseInt(rawDataArray[0]);
        graphObj.p = adjustPower(parseFloat(rawDataArray[1]));
        graphData.push(graphObj);
      }
      telemetryData.graph = graphData;

      var powerData = [];
      for (var j = 0; j < numOperands; j++) {
        var powerObj = {};
        var countLabel = j + 1;
        powerObj.label = "power " + countLabel;
        powerObj.status = getOperandStatus();
        powerData.push(powerObj);
      }

      telemetryData.power = powerData;

      var temperatureData = [];
      for (var j = 0; j < numOperands; j++) {
        var temperatureObj = {};
        var countLabel = j + 1;
        temperatureObj.label = "thermal " + countLabel;
        temperatureObj.status = getOperandStatus();
        temperatureData.push(temperatureObj);
      }
      telemetryData.temperature = temperatureData;

      try {
        var payload = JSON.stringify(telemetryData)
        ws.send(payload);
      } catch (e) {
        // Fail silently when client disconnects. No, this is not how it should really work.
      }
    },
    1000
  )

  function adjustPower(power) {
    var powerAdjustment = Math.floor(Math.random() * 3); // this will get a number between 1 and 99;
    if (powerAdjustment !== 0) {
      powerAdjustment *= Math.floor(Math.random() * 2) == 1 ? 1 : -1; // this will add minus sign in 50% of cases
    }
    var newPower = power + powerAdjustment;
    return Math.round(newPower * 10) / 10;
  }

  function getOperandStatus() {
    var rand = Math.floor(Math.random() * 100);
    var status = null;
    if (rand < 95) {
      return (status = "ok");
    } else if (rand < 96) {
      return (status = "standby");
    } else if (rand < 97) {
      return (status = "caution");
    } else if (rand < 98) {
      return (status = "error");
    } else if (rand < 99) {
      return (status = "off");
    } else {
      return (status = "emergency");
    }
  }

})