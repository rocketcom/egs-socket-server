const config = require('./config/config.json');
const _statusCategories = ["uca", "software", "rf", "digital", "comms", "facilities"];
const _errorSeverities = ["off", "standby", "caution", "serious", "alert"];
let WebSocketServer = require("ws").Server;
let Chance = require('chance');
let chance = new Chance();
let wss = new WebSocketServer({ port: config.ports.status });

console.log("EGS Status Server running on port %i", config.ports.status);

// Start functions
let getRandomIndex = (array) => {
  return Math.floor(Math.random() * Math.floor(array.length));
}
// End functions

wss.on('connection', function(ws) {
  let telemetryData = {};

  setInterval(
    function() {
      let d = new Date();
      let timestamp = d.getTime();
      let telemetryData = {};
      let ucaStatus = {};
      ucaStatus.worstStatus = _errorSeverities[getRandomIndex(_errorSeverities)];
      ucaStatus.numMessages = chance.integer({ min: 0, max: 50 });
      telemetryData.ucaStatus = ucaStatus;
      let softwareStatus = {};
      softwareStatus.worstStatus = _errorSeverities[getRandomIndex(_errorSeverities)];
      softwareStatus.numMessages = chance.integer({ min: 0, max: 50 });
      telemetryData.softwareStatus = softwareStatus;
      let rfStatus = {};
      rfStatus.worstStatus = _errorSeverities[getRandomIndex(_errorSeverities)];
      rfStatus.numMessages = chance.integer({ min: 0, max: 50 });
      telemetryData.rfStatus = rfStatus;
      let digitalStatus = {};
      digitalStatus.worstStatus = _errorSeverities[getRandomIndex(_errorSeverities)];
      digitalStatus.numMessages = chance.integer({ min: 0, max: 50 });
      telemetryData.digitalStatus = digitalStatus;
      let commsStatus = {};
      commsStatus.worstStatus = _errorSeverities[getRandomIndex(_errorSeverities)];
      commsStatus.numMessages = chance.integer({ min: 0, max: 50 });
      telemetryData.commsStatus = commsStatus;
      let facilitiesStatus = {};
      facilitiesStatus.worstStatus = _errorSeverities[getRandomIndex(_errorSeverities)];
      facilitiesStatus.numMessages = chance.integer({ min: 0, max: 50 });
      telemetryData.facilitiesStatus = facilitiesStatus;
      telemetryData.timestamp = timestamp;

      try {

        let payload = JSON.stringify(telemetryData);

        ws.send(payload);
      } catch (e) {
        // Fail silently when client disconnects. No, this is not how it should really work.
      }
    },
    10000
  )

});