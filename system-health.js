const config = require('./config/config.json');
const _errorSeverities = ["off", "standby", "caution", "serious", "critical"];
let WebSocketServer = require("ws").Server;
let Chance = require('chance');
let chance = new Chance();
let wss = new WebSocketServer({ port: config.ports.health });

console.log("EGS System Health Server running on port %i", config.ports.health);

// Start functions
let getRandomIndex = (array) => {
  return Math.floor(Math.random() * Math.floor(array.length));
}
// End functions

wss.on('connection', function(ws) {
  let healthData = {};
  let frameCount = 0;
  let maxFrameCount = 999999;
  let vccValue = 0;
  let maxVcc = 999999;
  let badCommandCount = 0;

  setInterval(
    function() {
      if ( frameCount < maxFrameCount ) {
        frameCount++;
      } else {
        frameCount = 0;
      }

      if ( vccValue < maxVcc ) {
        vccValue++;
        let randomBad = chance.integer({min:0, max: 20});
        if ( randomBad === 1 ) {
          badCommandCount++;
        }
      }
      let d = new Date();
      let healthData = {}
      try {
        let systems = {};
        let lock = {};
        let lockStatus = 'normal';
        lock.status = lockStatus;
        lock.value = chance.integer({min: -100, max: -97});
        lock.signalStrength = chance.floating({min: -110, max: -60, fixed: 2});
        systems.lock = lock;
        let telemetry = {};
        let telemetryStatus = 'normal';
        telemetry.status = telemetryStatus;
        telemetry.value = chance.integer({min: 96, max: 100});
        telemetry.frameCount = frameCount;
        systems.telemetry = telemetry;
        let vcc = {}
        let vccStatus = 'normal';
        vcc.status = vccStatus;
        vcc.value = vccValue;
        vcc.badCommandCount = badCommandCount;
        systems.vcc = vcc;
        healthData.systems = systems;
        let subsystems = {};
        let attitude = {};
        let attitudeStatus = 'caution';
        attitude.status = attitudeStatus;
        subsystems.attitude = attitude;
        let payload = {};
        let payloadStatus = 'normal';
        payload.status = payloadStatus;
        subsystems.payload = payload;
        let power = {};
        let powerStatus = 'normal';
        power.status = powerStatus;
        subsystems.power = power;
        let propulsion = {};
        let propulsionStatus = 'normal';
        propulsion.status = propulsionStatus;
        subsystems.propulsion = propulsion;
        let thermal = {};
        let thermalStatus = 'normal';
        thermal.status = thermalStatus;
        subsystems.thermal = thermal;
        healthData.subsystems = subsystems;

        let data = JSON.stringify(healthData);

        ws.send(data);
      } catch (e) {
        // Fail silently when client disconnects. No, this is not how it should really work.
      }
    },
    2000
  )

});