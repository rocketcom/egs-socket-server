# EGS Socket Server README
The Astro Sample Apps Suite relies on a number of Web Socket services to drive the applications. The default configuration of the application points to Rocket Communications' instances of these services; if you are able to access these through your firewall, you will not need to use the code in this repository. If, however, you need or want to run your own instances of the services, you've come to the right place. This repository contains all of the code and information you will need.

## Services Provided

### Alerts
The Alerts service populates the Alerts Panes in the GRM and TT&C Sample Applications.

### Status
The Status service drives the Monitoring Icons in the Global Status bar that appears in all Astro applications.

### Satellites
There are two Satellite services, each of which provides a basic set of data for the Telemetry section of the Astro Sample App which has representations of two satellites.

### System Health
The System Health service provides the data that drive the System Health and Subsystem panels in the right sidebar of the TT&C Command application.

## Prerequisites
1. A UNIX-like OS (Linux, Mac OS, FreeBSD, etc.)
2. [Node.js](https://nodejs.org/)
3. [npm](https://www.npmjs.com/get-npm)
4. [forever](https://www.npmjs.com/package/forever) node package (optional) 


## Installation
1. `git clone git@bitbucket.org:rocketcom/egs-socket-server.git`
2. `cd <directory-where-you-cloned-repository>`
3. `npm install`
4. `cd config`
5. `ln -s config.prod.json config.json`

If you wish to place these services behind a web server, you will need to configure the server to proxy-pass the requests to the appropriate ports on localhost. While instructions on setting up a web server are beyond the scope of this document, we have provided a sample configuration file named "sample-nginx-config" for the popular Nginx web server in the config directory in this repository.

## Running the Services
Each service runs on a dedicated port. You can choose to run the scripts in two ways:

1. `node <script-name.js>`
2. `forever start <script-name.js>`

The latter requires the optional node "forever" package mentioned above.

## Consuming the Services
Each Astro app has a config directory with config files for different environments that contains the URLs for each of the services. Simply create your own copy of one of these config files, adjust the URLs to match your environment, then symlink the new file to config.json.

## Support
We welcome your questions and comments! Please contact us at <UXSupport@rocketcom.com> and we'll get back to you shortly (generally within one business day).